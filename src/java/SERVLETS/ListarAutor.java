package SERVLETS;

import DAO.AutorDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Elaine / Endrigo
 */
public class ListarAutor implements Acao {
    
     public String executar(HttpServletRequest request, HttpServletResponse response){
        AutorDAO autor = new AutorDAO();
        
        request.setAttribute("autores", autor.retornaAutores());

        return "/AutorLista.jsp";
    }

}
