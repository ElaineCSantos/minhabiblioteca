package SERVLETS;

import DAO.AutorDAO;
import DAO.LivroDAO;
import bean.Autor;
import bean.Livro;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Elaine / Endrigo
 */
public class ExcluirAutor implements Acao {

    public String executar(HttpServletRequest request, HttpServletResponse response) {
        AutorDAO autorDAO = new AutorDAO();

        autorDAO.removerAutor(Integer.parseInt(request.getParameter("id")));

        return "/Controller?acao=ListarAutor";
    }

}
