package SERVLETS;

import DAO.LivroDAO;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Endrigo
 */
public class CadastrarLivro implements Acao {

    public String executar(HttpServletRequest request, HttpServletResponse response) {
        LivroDAO livroDAO = new LivroDAO();
        List<Integer> autores = new ArrayList();
        try {
            String autoresArray[] = request.getParameterValues("autores");

            for (int i = 0; i < autoresArray.length; i++) {
                autores.add(Integer.parseInt(autoresArray[i]));
            }
        } catch (Exception e) {
        }
        String id = request.getParameter("id");
        if (id == null || id.isEmpty() || id.equalsIgnoreCase("0")) {
            livroDAO.adicionarLivro(autores, request.getParameter("titulo"), Integer.parseInt(request.getParameter("edicao")));
        } else {
            livroDAO.atualizarLivro(Integer.parseInt(id), autores, request.getParameter("titulo"), Integer.parseInt(request.getParameter("edicao")));
        }

        return "/Controller?acao=ListarLivro";
    }
}
