package SERVLETS;

import DAO.AutorDAO;
import bean.Autor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Endrigo
 */
public class CadastrarAutor implements Acao {
    public String executar(HttpServletRequest request, HttpServletResponse response){
        AutorDAO autorDAO = new AutorDAO();
        Autor autor = new Autor(request.getParameter("nome"));
        
        String id = request.getParameter("id");
        if (id == null || id.isEmpty() || id.equalsIgnoreCase("0")) {
            autorDAO.adicionarAutor(autor);
        } else {
            autorDAO.atualizarAutor(Integer.parseInt(id),request.getParameter("nome"));
        }
        
        return "/Controller?acao=ListarAutor";
    }
}
