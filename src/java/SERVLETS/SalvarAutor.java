package SERVLETS;

import DAO.AutorDAO;
import DAO.LivroDAO;
import bean.Autor;
import bean.Livro;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Elaine / Endrigo
 */
public class SalvarAutor implements Acao {

    public String executar(HttpServletRequest request, HttpServletResponse response) {
        
        AutorDAO autorDAO = new AutorDAO();
        String id = request.getParameter("id");

        Autor autor;
        
        if (id == null || id == "") {
            autor = new Autor();
        } else {
            autor = autorDAO.retornaAutor(Integer.parseInt(id));
        }

        request.setAttribute("autor", autor);

        return "/AutorCadastro.jsp";
    }

}
