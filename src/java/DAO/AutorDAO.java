package DAO;

import bean.Autor;
import bean.Livro;
import fabricaDeSessoes.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author endrigo
 */
public class AutorDAO {

    static SessionFactory fabrica = HibernateUtil.getSessionFactory();

    public void adicionarAutor(Autor autor) {

        Session session = fabrica.openSession();
        session.beginTransaction();

        session.save(autor);

        session.getTransaction().commit();
        session.close();
    }

    public Autor retornaAutor(int id) {
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Autor where id = :autor");
        Autor autor = (Autor) q.setParameter("autor", id).uniqueResult();
        session.close();
        return autor;
    }

    public List<Autor> retornaAutores() {

        List<Autor> autores;
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Autor");
        autores = q.list();
        session.close();

        return autores;
    }

    public void atualizarAutor(int id, String nome) {

        Autor autor = this.retornaAutor(id);
        autor.setNome(nome);

        Session session = fabrica.openSession();
        session.beginTransaction();

        session.update(autor);

        session.getTransaction().commit();
        session.close();
    }

    public void removerAutor(int id) {
        Autor autor = this.retornaAutor(id);
        List<Livro> livros = autor.getLivros();
        for (Livro livro : autor.getLivros()) {
            livro.getAutor().remove(autor);
        }

        Session session = fabrica.openSession();
        if (livros.size() > 0) {
            session.beginTransaction();

            for (Livro livro : livros) {
                session.update(livro);
            }

            session.getTransaction().commit();
        }
        session.beginTransaction();
        session.delete(autor);
        session.getTransaction().commit();

        session.close();
    }
}
