<%-- 
    Document   : LivroLista
    Created on : 26/03/2017, 20:56:20
    Author     : endrigo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Livros</title>
        <style>
            table tr{
                text-align: left!important;
                
            }
            table, table tr td, table tr th{
                border: solid 1px;
            }
        </style>
    </head>
    <body>
        <h1>Livros</h1>
        <table>
            <tr>
                <th>Id</th>                
                <th>Título</th>
                <th>Edição</th>
                <th>Autor(es)</th>
            </tr>
        <c:forEach var="livro" items="${livros}">
            <tr>
                <td>
                    <a href="Controller?acao=SalvarLivro&id=${livro.id}">${livro.id}</a>                    
                </td>
                <td>
                    ${livro.titulo}
                </td>
                <td>
                    ${livro.edicao}
                </td>
                <td>
                    ${livro.autor}
                </td>
                <td>
                    <a href="Controller?acao=ExcluirLivro&id=${livro.id}">Excluir</a>                    
                </td>
            </tr>
            
        </c:forEach>
        </table>
        <br></br>
        <a href="index.html">Voltar</a>
        </body>
</html>
